import React from 'react';
import { StyleSheet } from "react-native";
import { COLORS, FONT, SIZES } from "../../constants";



const styles = StyleSheet.create({
    
    container: {
        borderTopWidth: 1,
    },
    

    FooterStyle: {
        verticalAlign: 'bottom',
        flexDirection: 'row',
        marginTop: 10,
        justifyContent: 'center'
    },

});

export default styles;
